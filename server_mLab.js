require('dotenv').config();
const express = require('express'); //import
const body_parser = require('body-parser'); //agregado
const request_json = require('request-json');
const app = express();
//const port = 3000;
const port = process.env.PORT || 3000;
const URL_BASE = '/techu/v2/';
const usersFile = require('./user.json');
const URL_mLab = 'https://api.mlab.com/api/1/databases/techu20db/collections/';
//const apikey_mLab = 'apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF';
const apikey_mLab = 'apiKey=' + process.env.API_KEY_MLAB;
//app.listen(port);
//  console.log('Node JS escuchando en el puerto ' + port);
//var client = request.createClient(URL_mLab + 'apikey_mLab');
app.listen(port,function(){
  console.log('Node JS escuchando en el puerto ' + port);
});
app.use(body_parser.json());
//agregado
//operacion get con mlab collection
app.get(URL_BASE + 'users',
      function(req, res){
       const http_client = request_json.createClient(URL_mLab);
       // + '?' + apikey_mLab);
  console.log('Cliente HTTP a mLab creado correctamente');
  const field_param = 'f={"_id":0}&';
  http_client.get('user?'+ field_param + apikey_mLab,
  function(err, respuestaMLab, body) {
     console.log('Error: ' + err);
     console.log('Respuesta MLab: ' + respuestaMLab);
     console.log('Body: ' + body);
     var response = {};
        if(err) {
                response = {"msg" : "Error al recuperar users de mLab."}
                res.status(500);
            } else {
              if(body.length > 0) {
                response = body;
              } else {
                response = {"msg" : "Usuario no encontrado."};
                res.status(404);
              }
            }
            res.send(response);
          }); //httpClient.get(
    });

// GET users con ID
app.get(URL_BASE + 'users/:id_user',
      function(req, res){
        console.log('id:' + req.params.id_user);
          var id= req.params.id_user;
        //  var queryString = 'q={"id_user": ${id}}&';
          var queryString = 'q={"id_user":' + id +'}&';
      //   let  queryString = 'q={"id_user": ${id_user}}&';
          var queryStrField = 'f={"_id":0}&';
          var httpClient = request_json.createClient(URL_mLab);
          httpClient.get('user?' + queryString + queryStrField +  apikey_mLab,
            function(error, res_mlab, body){
              console.log("Repuesta mLab correcta. ");
              console.log(error);
            //  console.log('res_mlab: ${JSON.stringify(res_mlab)}');
            var response = {};
           if(error) {
              response = {"msg" : "Error obteniendo usuario."};
              res.status(500);
          } else {
            if(body.length > 0){
              response = body;
           }  else  {
             response = {"msg" : "Usuario no encontrado."};
             res.status(404);
          }
        }
         res.send(response);
      });
});
//GET cuentas del usuario con parametro
 app.get(URL_BASE + 'users/:id_user/account', // GET users/4/account
  function(req, res){
    console.log('GET users/:id_user/account');
    let id=req.params.id_user;
  //  var queryString = 'q={"id_user": ${id} }&';
    //let queryStrField = 'f={"_id":0}&';
    let queryStrField = 'f={"account":1, "_id":0}&';
    let  queryString = 'q={"id_user":' + id +'}&';
    var httpClient = request_json.createClient(URL_mLab);
    console.log(id);
  //  httpClient.get('account?' + queryString + queryStrField +apikey_mLab,
  httpClient.get('user?' + queryString + queryStrField +apikey_mLab,
    function(error, res_mlab, body){
      let respuesta = {};
      if(error) {
         respuesta = {"msg" : "Error al recuperar cuenta de mLab."};
         res.status(500);
     } else { console.log(body.length);
       if(body.length > 0){
         respuesta = body;
      }  else  {
        respuesta = {"msg" : "Usuario no encontrado."};
        res.status(404);
     }
   }
  //   respuesta = !error ? body : {'msg':'usuario no tiene cuentas'};
     res.send(respuesta);
   }) ;
 });

   //POST of user
app.post(URL_BASE + 'usuarios',
 function(req, res) {
  var  clienteMlab = request_json.createClient(URL_mLab);
  clienteMlab.get('usuario?'+ apikey_mLab ,
  function(error, respuestaMLab , body) {
    let  newID=body.length+1;
      console.log("newID:" + newID);
      var newUser = {
        "id" : newID+1,
        "first_name" : req.body.first_name,
        "last_name" : req.body.last_name,
        "email" : req.body.email,
        "password" : req.body.password
      };
      clienteMlab.post(URL_mLab + "usuario?" + apikey_mLab, newUser ,
       function(error, respuestaMLab, body) {
        res.send(body);
     });
  });
});

// PUT users con parámetro 'id'
app.put(URL_BASE + 'users/:id_user',
function(req, res) {
  var id = req.params.id_user;
  var queryStringID = 'q={"id":' + id + '}&';
  var clienteMlab = request_json.createClient(URL_mLab);
  clienteMlab.get('user?'+ queryStringID + apikey_mLab,
    function(error, respuestaMLab, body) {
     var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
     console.log(cambio);
     clienteMlab.put(URL_mLab +'user?' + queryStringID + apikey_mLab, JSON.parse(cambio),
      function(error, respuestaMLab, body) {
        console.log("body:"+ body); // body.n == 1 si se pudo hacer el update
       //res.status(200).send(body);
       res.send(body);
      });
    });
});
// Petición PUT con id de mLab (_id.$oid)
  app.put(URL_BASE + 'users/:id_user',
    function (req, res) {
      var id = req.params.id_user;
      let userBody = req.body;
      var queryString = 'q={"id":' + id + '}&';
      var httpClient = request_json.createClient(URL_mLab);
      httpClient.get('user?' + queryString + apikey_mLab,
        function(err, respuestaMLab, body){
          let response = body[0];
          console.log(body);
          //Actualizo campos del usuario
          let updatedUser = {
            "id" : req.body.id_user,
            "first_name" : req.body.first_name,
            "last_name" : req.body.last_name,
            "email" : req.body.email,
            "password" : req.body.password
          };//Otra forma optimizada (para muchas propiedades)
          // var updatedUser = {};
          // Object.keys(response).forEach(key => updatedUser[key] = response[key]);
          // Object.keys(userBody).forEach(key => updatedUser[key] = userBody[key]);
          // PUT a mLab
          httpClient.put('user/' + response._id.$oid + '?' + apikey_mLab, updatedUser,
            function(err, respuestaMLab, body){
              var response = {};
              if(err) {
                  response = {
                    "msg" : "Error actualizando usuario."
                  }
                  res.status(500);
              } else {
                if(body.length > 0) {
                  response = body;
                } else {
                  response = {
                    "msg" : "Usuario actualizado correctamente."
                  }
                  res.status(404);
                }
              }
              res.send(response);
            });
        });
  });
//DELETE user with id
app.delete(URL_BASE + 'users/:id_user',
  function(req, res){
    console.log("entra al DELETE");
    console.log("request.params.id: " + req.params.id_user);
    var id = req.params.id_user;
    var queryStringID = 'q={"id":' + id + '}&';
    console.log(URL_mLab + 'user?' + queryStringID + apikey_mLab);
    var httpClient = request_json.createClient(URL_mLab);
    httpClient.get('user?' +  queryStringID + apikey_mLab,
      function(error, respuestaMLab, body){
        var respuesta = body[0];
        console.log("body delete:"+ JSON.stringify(respuesta));
        httpClient.delete(URL_mLab + "user/" + respuesta._id.$oid +'?'+ apikey_mLab,
          function(error, respuestaMLab,body){
            res.send(body);
        });
      });
  });
// DELETE con PUT de mLab
app.delete(URL_BASE + "users/:id_user",
  function(req, res){
    var id = Number.parseInt(req.params.id_user);
    var query = 'q={"id":' + id + '}&';
    // Interpolación de expresiones
    var query2 = `q={"id":${id} }&`;
    var query3 = "q=" + JSON.stringify({"id": id})
 + '&';
    httpClient = request_json.createClient(URL_mLab);
    httpClient.put("user?" + query + apikey_mLab, [{}],
        function(err, resMLab, body) {
          var response = !err ? body : {
            "msg" : "Error borrando usuario."
          }
        res.send(response);
      }
    );
  });

  //Method POST login
app.post(URL_BASE + "login",
  function (req, res){
    console.log("POST /tech/v2/login");
    let email = req.body.email;
    let pass = req.body.password;
    let queryString = 'q={"email":"' + email + '","password":"' + pass + '"}&';
    let limFilter = 'l=1&';
    let clienteMlab = request_json.createClient(URL_mLab);
    clienteMlab.get('user?'+ queryString + limFilter + apikey_mLab,
      function(error, respuestaMLab, body) {
        if(!error) {
          if (body.length == 1) { // Existe un usuario que cumple 'queryString'
            let login = '{"$set":{"logged":true}}';
            clienteMlab.put('user?q={"id": ' + body[0].id + '}&' + apikey_mLab, JSON.parse(login),
            //clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),
              function(errPut, resPut, bodyPut) {
                res.send({'msg':'Login correcto', 'user':body[0].email, 'userid':body[0].id, 'name':body[0].first_name});
                // If bodyPut.n == 1, put de mLab correcto
              });
          }
          else {
            res.status(404).send({"msg":"Usuario no válido."});
          }
        } else {
          res.status(500).send({"msg": "Error en petición a mLab."});
        }
    });
});
//Method POST logout
app.post(URL_BASE + "logout",
  function(req, res) {
    console.log("POST /tech/v3/logout");
    var email = req.body.email;
    var queryString = 'q={"email":"' + email + '","logged":true}&';
    console.log(queryString);
    var  clienteMlab = request_json.createClient(URL_mLab);
    clienteMlab.get('user?'+ queryString + apikey_mLab,
      function(error, respuestaMLab, body) {
        var respuesta = body[0]; // Asegurar único usuario
        if(!error) {
          if (respuesta != undefined) { // Existe un usuario que cumple 'queryString'
            let logout = '{"$unset":{"logged":true}}';
            clienteMlab.put('user?q={"id": ' + respuesta.id_user + '}&' + apikey_mLab, JSON.parse(logout),
            //clienteMlab.put('user/' + respuesta._id.$oid + '?' + apikeyMLab, JSON.parse(logout),
              function(errPut, resPut, bodyPut) {
                res.send({'msg':'Logout correcto', 'user':respuesta.email});
                // If bodyPut.n == 1, put de mLab correcto
              });
            } else {
                res.status(404).send({"msg":"Logout failed!"});
            }
        } else {
          res.status(500).send({"msg": "Error en petición a mLab."});
        }
    });
});
