require('dotenv').config();
const express = require('express'); //import
const body_parser = require('body-parser'); //agregado
const app = express();
//const port = 3000;
const port = process.env.PORT || 3000;
const URL_BASE = '/techu/v1/';
const usersFile = require('./user.json');
//app.listen(port);
//  console.log('Node JS escuchando en el puerto ' + port);

app.listen(port,function(){
  console.log('Node JS escuchando en el puerto ' + port);
});
app.use(body_parser.json()); //agregado


app.get(URL_BASE + 'users',
      function(request, response){
  response.status(200).send(usersFile);
  });
// Operacion GET Hola Mundo
//app.get('/holamundos',
// Operacion GET (Collection)
app.get(URL_BASE + 'users',
      function(request, response){
    //    console.log("Hola Mundo amistades");
    //    console.log("Hola Lima-mañana bonita");
    //    response.send('Hola amigos gracias por todo');// la respuesta (send) al final
    // response.send({'usuario' : 'Vaneza Ortiz Lobato'});
     response.send(usersFile);
});

// Peticion GET a un unico usuario mediante ID (Instancia)

app.get(URL_BASE + 'users/:id',
//app.get(URL_BASE + 'users/:id/:p1/:p2',
  function(request, response){
    console.log('id:' + request.params.id);
    //Enviar respuesta al cliente
    let pos = request.params.id - 1;
    let respuesta = (usersFile[pos]== undefined) ?{"msg":"usuario no existente"} : usersFile[pos];
    let status_code = (usersFile[pos] == undefined)  ? 404 :200;
    response.status(status_code).send(respuesta);

  //  response.send(respuesta);
  });

  // GET con query string

  app.get(URL_BASE + 'usersq',
  function(req, res){
    console.log(req.query.id);
    console.log(req.query.country);
    let respuesta = (req.query.id == undefined) ?{"msg":"Id no existente"} : req.query.id;
    //res.send({"msg" : "GET con query"});
    response.send(respuesta);
  });


  //Peticion POST a users
app.post(URL_BASE + 'users',
function(req, res){
  console.log('POST a users');
   let tam = usersFile.length;
   let new_user = {
     "id_user":tam + 1,
     "first_name":req.body.first_name,
     "last_name":req.body.last_name,
     "email":req.body.email,
     "password":req.body.password
    }
    console.log(new_user);
    usersFile.push(new_user);
    let respuesta = (req.query.id == undefined) ?{"msg":"Id no existente"} : req.query.id;
    res.send({"msg": "Usuario creado correctamente"});
});

  //Peticion PUT a user

  app.put(URL_BASE + 'users/:id',
 function(req, res){
   console.log('PUT a users');
    // let tam = usersFile.length;
    console.log('id:' + req.params.id);
        let pos = req.params.id - 1;
  //  response.send(usersFile[pos]);
     let act_user = {
        "id_user": req.params.id,
       "first_name":req.body.first_name,
       "last_name":req.body.last_name,
       "email":req.body.email,
       "password":req.body.password
      }
      console.log(act_user);
      usersFile[pos]=act_user;
     res.send({"msg": "Usuario actualizado correctamente"});
  });

  // Peticion DELETE a users (mediante su ID)
  app.delete(URL_BASE + 'users/:id',
 function(req, res){
   console.log('delete a users');
  // let pos = req.params.id - 1;
  // let del_user = {
  //    "id_user": req.params.id,
  //   "first_name":req.body.first_name,
  //   "last_name":req.body.last_name,
  //   "email":req.body.email,
  //   "password":req.body.password
  //  }
  //  console.log(del_user);
    usersFile.splice(req.params.id -1,1);
    res.send({"msg": "Usuario eliminado correctamente"});
   });

   //

   // LOGIN - users.json
app.post(URL_BASE + 'login',
  function(request, response) {
    console.log("POST /apicol/v2/login");
    console.log(request.body.email);
    console.log(request.body.password);
    var user = request.body.email;
    var pass = request.body.password;
    for(us of usersFile) {
      if(us.email == user) {
        if(us.password == pass) {
          us.logged = true;
          writeUserDataToFile(usersFile);
          console.log("Login correcto!");
          response.send({"msg" : "Login correcto.", "idUsuario" : us.id, "logged" : "true"});
        } else {
          console.log("Login incorrecto.");
          response.send({"msg" : "Login incorrecto."});
        }
      }
    }
});

function writeUserDataToFile(data) {
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./users.json", jsonUserData, "utf8",
   function(err) { //función manejadora para gestionar errores de escritura
     if(err) {
       console.log(err);
     } else {
       console.log("Datos escritos en 'users.json'.");
     }
   }); }


   // LOGOUT - users.json
// app.post(URL_BASE + 'logout',
//  function(request, response) {
//    console.log("POST /apicol/v2/logout");
//    var userId = request.body.id;
//    for(us of usersFile) {
//      if(us.id == userId){

//        if(us.logged) {
//          delete us.logged; // borramos propiedad 'logged'
//          writeUserDataToFile(usersFile);
//          console.log("Logout correcto!");
//          response.send({"msg" : "Logout correcto.", "idUsuario" : us.id});
//        } else {
//          console.log("Logout incorrecto.");
//          response.send({"msg" : "Logout incorrecto."});
//        }
//      }  us.logged = true
//    }
//});
